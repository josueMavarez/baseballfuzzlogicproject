package baseballproject;

import easyfuzzy.controller.BasicFuzzyController;
import easyfuzzy.controller.FuzzyOp;
import easyfuzzy.defuzzifyer.CentroidMethod;
import easyfuzzy.rules.modifier.FzSet;
import easyfuzzy.variables.IllegalSetException;
import easyfuzzy.variables.LinguisticVariable;
import easyfuzzy.variables.functions.FunctionException;
import easyfuzzy.variables.functions.TriangularMembershipFunction;

/**
 *
 * @author Josue Mavarez
 *         Eugenio Mendoza
 *         Angelica Pavon
 *         Irosshi Paz
 *         Marco Portillo
 */

public class BaseballProject {
    public static final String ALTURA = "ALTURA";
    public static final String DISTANCIA = "DISTANCIA";
    public static final String VELOCIDAD = "VELOCIDAD";
    
    /* Variables linguisticas Altura */
    /* Variables de Entrada */
    private FzSet pequenoVL;
    private FzSet medianoVL;
    private FzSet grandeVL;
    /* String tags para identificar las variables */
    private static final String pequenoString = "pequeño";
    private static final String medianoString = "mediano"; 
    private static final String grandeString = "grande";  
    
    /* Variables linguisticas para Distancia */
    /* Variables de Entrada */
    private FzSet muycercaVL;
    private FzSet cercaVL;
    private FzSet medioVL;
    private FzSet lejosVL;
    private FzSet muylejosVL;
    /* String tags para identificar las variables */
    private static final String muycercaString = "muycerca"; 
    private static final String cercaString = "cerca"; 
    private static final String medioString = "medio"; 
    private static final String lejosString = "lejos"; 
    private static final String muylejosString = "muylejos"; 
    
    /* Variables linguisticas para VELOCIDAD */
    /* Variables de Salida */
    private FzSet muylentoVL;
    private FzSet lentoVL;
    private FzSet mMedioVL;
    private FzSet rapidoVL;
    private FzSet muyrapidoVL;
    /* String tags para identificar las variables */
    private static final String muylentoString = "muylento"; 
    private static final String lentoString = "lento"; 
    private static final String mMedioString = "mMedio"; 
    private static final String rapidoString = "rapido"; 
    private static final String muyrapidoString = "muyrapido"; 
    
    /**
     * @param args the command line arguments
     * @throws easyfuzzy.variables.IllegalSetException
     * @throws easyfuzzy.variables.functions.FunctionException
     */
    public static void main(String[] args) throws IllegalSetException, FunctionException {
        /* Inicializar nuevo controlador para logica difusa */
        BasicFuzzyController bfc = new BasicFuzzyController();
        
        /* Instanciacion de la misma clase para llamar metodos de tipo static */ 
        BaseballProject baseballProject = new BaseballProject();
        
        /* Configurar variables linguisticas */
        baseballProject.setVariables(bfc);
        
        /* Configurar reglas linguisticas */
        baseballProject.setReglas(bfc);
        
        /* Input */
        bfc.fuzzify(ALTURA, 1.9); // INPUT < 2
        bfc.fuzzify(DISTANCIA, 9.9999999); // INPUT < 10
        
//        CentroidMethod cm = new CentroidMethod();
//        cm.setSamplesPoints(10);
//        bfc.setDefuzzifyerMethod(cm);
        System.out.println("DEFUSING VALUE :V " + bfc.defuzzify(VELOCIDAD) * 1.60934 + " Km/s");
    }
    
    void setVariables(BasicFuzzyController bfc) throws IllegalSetException, FunctionException {
            /* Nueva variables linguistica */
            LinguisticVariable lvAltura = new LinguisticVariable(ALTURA);
            
            /* SubVariables y sus rangos */
            /* Funcion triangular A = inicio, B = fin, c = punto medio */
            pequenoVL = lvAltura.addSet(pequenoString, new TriangularMembershipFunction(1.33, 1.52, 1.425));
            medianoVL = lvAltura.addSet(medianoString, new TriangularMembershipFunction(1.53, 1.76, 1.645));
            grandeVL = lvAltura.addSet(grandeString, new TriangularMembershipFunction(1.77, 2, 1.885));
            /* Agregar variable al controlador */
            bfc.addVariable(lvAltura);
            
            /* Nueva variables linguistica */
            LinguisticVariable lvDistancia = new LinguisticVariable(DISTANCIA);
            
            /* SubVariables y sus rangos */
            muycercaVL = lvDistancia.addSet(muycercaString, new TriangularMembershipFunction(2, 3.5, 2.75));
            cercaVL = lvDistancia.addSet(cercaString, new TriangularMembershipFunction(3.6, 5.1, 4.35));
            medioVL = lvDistancia.addSet(medioString, new TriangularMembershipFunction(5.2, 6.7, 5.95));
            lejosVL = lvDistancia.addSet(lejosString, new TriangularMembershipFunction(6.8, 8.3, 7.55));
            muylejosVL = lvDistancia.addSet(muylejosString, new TriangularMembershipFunction(8.4, 10, 9.2));
            /* Agregar variable al controlador */
            bfc.addVariable(lvDistancia);
            
            /* SubVariables y sus rangos */
            LinguisticVariable lvVelocidad = new LinguisticVariable(VELOCIDAD);
            
            /* SubVariables y sus rangos */
            muylentoVL = lvVelocidad.addSet(muylentoString, new TriangularMembershipFunction(30, 40, 35));
            lentoVL = lvVelocidad.addSet(lentoString, new TriangularMembershipFunction(41, 51, 46));
            mMedioVL = lvVelocidad.addSet(mMedioString, new TriangularMembershipFunction(52, 62, 57));
            rapidoVL = lvVelocidad.addSet(rapidoString, new TriangularMembershipFunction(63, 73, 68));
            muyrapidoVL = lvVelocidad.addSet(muyrapidoString, new TriangularMembershipFunction(74, 85, 79.5));
            /* Agregar variable al controlador */
            bfc.addVariable(lvVelocidad);
            System.out.println("VARIABLES CREATED!!!");
    }
    
    void setReglas(BasicFuzzyController bfc) {
        /* addRule(FuzzyOp.and(a, b), c) 
        Definicion de reglas a = Entrada A; b = Entrada B; c = Salida */
        
        bfc.addRule(FuzzyOp.and(muycercaVL, pequenoVL), muylentoVL);
        bfc.addRule(FuzzyOp.and(muycercaVL, medianoVL), lentoVL);
        bfc.addRule(FuzzyOp.and(muycercaVL, grandeVL), lentoVL);
        
        bfc.addRule(FuzzyOp.and(cercaVL, pequenoVL), muylentoVL);
        bfc.addRule(FuzzyOp.and(cercaVL, medianoVL), mMedioVL);
        bfc.addRule(FuzzyOp.and(cercaVL, grandeVL), mMedioVL);
        
        bfc.addRule(FuzzyOp.and(medioVL, pequenoVL), lentoVL);
        bfc.addRule(FuzzyOp.and(medioVL, medianoVL), mMedioVL);
        bfc.addRule(FuzzyOp.and(medioVL, grandeVL), rapidoVL);
        
        bfc.addRule(FuzzyOp.and(lejosVL, pequenoVL), mMedioVL);
        bfc.addRule(FuzzyOp.and(lejosVL, medianoVL), rapidoVL);
        bfc.addRule(FuzzyOp.and(lejosVL, grandeVL), muyrapidoVL);
        
        bfc.addRule(FuzzyOp.and(muylejosVL, pequenoVL), rapidoVL);
        bfc.addRule(FuzzyOp.and(muylejosVL, medianoVL), muyrapidoVL);
        bfc.addRule(FuzzyOp.and(muylejosVL, grandeVL), muyrapidoVL);
        System.out.println("RULES CREATED!!!");
    }  
}